<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;
use App\Models\Customer;
use App\Http\Resources\CustomerResource;


class CustomerController extends Controller
{
    //retrieve data from api to database  
    public function index()

    {
        $client = new Client();
        $url = "https://jsonplaceholder.typicode.com/users";

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $jsonData = json_decode($response->getBody());

        foreach ($jsonData as $key => $value) {
            $customer = Customer::create([
                'name' => $value->name,
                'username' => $value->username,
                'email' => $value->email,
                'street' => $value->address->street,
                'suite' => $value->address->suite,
                'city' => $value->address->city,
                'zipcode' => $value->address->zipcode,
                'geo_lat' => $value->address->geo->lat,
                'geo_lng' => $value->address->geo->lng,
                'phone' => $value->phone,
                'website' => $value->website,
                'company_name' => $value->company->name,
                'catchPhrase' => $value->company->catchPhrase,
                'bs' => $value->company->bs,

            ]);
        }
        return response(['message' => 'Created successfully']);
    }
    //update customer data
    public function update(Request $request, $id)
    {
        $data = $request->all();
         //this validate is an example but several exist.
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|unique:users',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
        }
        $customer = Customer::find($id);
        if (empty($customer)) {
            return response(['message' => 'This User does not exist, check your details'], 400);
        }
        $customer->update($data);

        return response(['customer' => new CustomerResource($customer), 'message' => 'Update successfully'], 200);
    }

    //search for name
    function search($name)
    {
        return Customer::where("name", $name)->get();
    }
}
