<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username');
            $table->text('email');
            $table->text('street');
            $table->text('suite');
            $table->text('city');
            $table->string('zipcode');
            $table->string('geo_lat');
            $table->string('geo_lng');
            $table->string('phone');
            $table->text('website');
            $table->string('company_name');
            $table->text('catchPhrase');
            $table->text('bs');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
